/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import net.minecraft.world.biome.Biome.Category;

public class Config {
	private static final int VERSION = 10;
	private static Config values;

	private int version;
	private float thornsDamage = 1.0f;
	private GenerationSettings generation = new GenerationSettings();
	
	private Config() {
		// add default config
		BiomeGroupConfig bgcDefault = new BiomeGroupConfig();
		bgcDefault.config = BiomeConfig.builder().stoneSpeleothems(true).skulls(true).cobwebs(true).spiderDens(true).leafPiles(true).build();
		generation.biomeGroups.add(bgcDefault);
		
		BiomeGroupConfig bgcDesert = new BiomeGroupConfig(Category.DESERT, Category.MESA);
		bgcDesert.config = BiomeConfig.builder().stoneSpeleothems(true).skulls(true).cobwebs(true).spiderDens(true).leafPiles(true).sandReplacement(true).sandstoneSpeleothems(true).build();
		generation.biomeGroups.add(bgcDesert);
		
		BiomeGroupConfig bgcCold = new BiomeGroupConfig(Category.ICY, Category.TAIGA);
		bgcCold.config = BiomeConfig.builder().stoneSpeleothems(true).skulls(true).cobwebs(true).spiderDens(true).leafPiles(true).iceReplacement(true).blueGlowshrooms(true).icicles(true).build();
		generation.biomeGroups.add(bgcCold);
		
		BiomeGroupConfig bgcJungle = new BiomeGroupConfig(Category.JUNGLE);
		bgcJungle.config = BiomeConfig.builder().stoneSpeleothems(true).skulls(true).cobwebs(true).spiderDens(true).leafPiles(true).vines(true).greenGlowshrooms(true).tallGreenGlowshrooms(true).largeCaves(true).build();
		generation.biomeGroups.add(bgcJungle);
		
		BiomeGroupConfig bgcForest = new BiomeGroupConfig(Category.FOREST);
		bgcForest.config = BiomeConfig.builder().stoneSpeleothems(true).skulls(true).cobwebs(true).spiderDens(true).leafPiles(true).wildBeetroot(true).build();
		generation.biomeGroups.add(bgcForest);
		
		BiomeGroupConfig bgcPlains = new BiomeGroupConfig(Category.PLAINS);
		bgcPlains.config = BiomeConfig.builder().stoneSpeleothems(true).skulls(true).cobwebs(true).spiderDens(true).leafPiles(true).wildWheat(true).wildPotatoes(true).wildCarrots(true).build();
		generation.biomeGroups.add(bgcPlains);
		
		BiomeGroupConfig bgcMountains = new BiomeGroupConfig(Category.EXTREME_HILLS);
		bgcMountains.includedBiomes.add("minecraft:badlands_plateau");
		bgcMountains.includedBiomes.add("minecraft:modified_badlands_plateau");
		bgcMountains.includedBiomes.add("minecraft:wooded_badlands_plateau");
		bgcMountains.includedBiomes.add("minecraft:modified_wooded_badlands_plateau");
		bgcMountains.config = BiomeConfig.builder().stoneSpeleothems(true).skulls(true).cobwebs(true).spiderDens(true).leafPiles(true).largeCaves(true).build();
		generation.biomeGroups.add(bgcMountains);
		
		BiomeGroupConfig bgcHumid = new BiomeGroupConfig(Category.SWAMP);
		bgcHumid.config = BiomeConfig.builder().stoneSpeleothems(true).skulls(true).cobwebs(true).spiderDens(true).leafPiles(true).greenGlowshrooms(true).tallGreenGlowshrooms(true).build();
		generation.biomeGroups.add(bgcHumid);
		
		BiomeGroupConfig bgcNether = new BiomeGroupConfig(Category.NETHER);
		bgcNether.config = BiomeConfig.builder().netherThorns(true).build();
		generation.biomeGroups.add(bgcNether);
		
		BiomeGroupConfig bgcEnd = new BiomeGroupConfig(Category.THEEND);
		bgcEnd.config = BiomeConfig.builder().endThorns(true).build();
		generation.biomeGroups.add(bgcEnd);
	}

	public static void init(File configDir) throws IOException {
		values = new Config();
		values.version = VERSION;
		
		// loading or creating config file
		if(!configDir.exists()) {
			configDir.mkdirs();
		}
		boolean writeConfig = false;
		File configFile = new File(configDir, "config.json");
		Gson gson = new Gson();
		if(configFile.exists()) {
			try(Reader reader = new FileReader(configFile)) {
				values = gson.fromJson(reader, Config.class);
			}
			
			// create a backup and revert to default config if config version is outdated
			if(values.version < VERSION) {
				File configFileBackup = new File(configDir, "config_old.json");
				Files.copy(configFile.toPath(), configFileBackup.toPath(), StandardCopyOption.REPLACE_EXISTING);
				System.out.println("WARNING: Config file for WildWorld was outdated:");
				System.out.println("Moved backup to config_old.json and reverted to default config!");
				values = new Config();
				values.version = VERSION;
				writeConfig = true;
			}
		} else {
			writeConfig = true;
		}
		
		if(writeConfig) {
			try(JsonWriter writer = new JsonWriter(new FileWriter(configFile))) {
				writer.setIndent("\t");
				gson.toJson(values, Config.class, writer);
			}
		}
	}
	
	public static float getThornsDamage() {
		return values.thornsDamage;
	}
	
	public static GenerationSettings getGenerationSettings() {
		return values.generation;
	}
	
	public static List<BiomeGroupConfig> getBiomeGroupConfigs() {
		return values.generation.biomeGroups;
	}
	
	public static class GenerationSettings {
		private @Getter float largeCaveChance = 0.2f;
		
		private @Getter int stoneSpeleothemCount = 128; // was 200, but minecraft now limits this to [-10,128]
		private @Getter int stoneSpeleothemMaxLength = 4;
		private @Getter int sandstoneSpeleothemCount = 128; // was 150, but minecraft now limits this to [-10,128]
		private @Getter int sandstoneSpeleothemMaxLength = 4;

		private @Getter float sandReplacementPercentage = 0.75f;
		private @Getter float iceReplacementPercentage = 0.5f;

		private @Getter int icicleCount = 128;

		private @Getter int skullCount = 3;
		private @Getter int cobwebCount = 40;
		private @Getter int spiderDenChance = 1;

		private @Getter int vineCount = 50;
		private @Getter int vineMaxHeight = 64;

		private @Getter int greenGlowshroomCount = 24;
		private @Getter int blueGlowshroomCount = 48;
		private @Getter int tallGreenGlowshroomCount = 6;
		private @Getter int tallGreenGlowshroomMaxHeight = 48;
		
		private @Getter int leafPileCount = 50;
		
		private @Getter int wildWheatCount = 2;
		private @Getter int wildPotatoeCount = 1;
		private @Getter int wildCarrotCount = 1;
		private @Getter int wildBeetrootCount = 2;
		
		private @Getter int netherThornCount = 16;
		private @Getter int endThornCount = 1;

		private List<BiomeGroupConfig> biomeGroups = new ArrayList<>();
	}

	public static @Data @Builder class BiomeConfig {
		private @Builder.Default final boolean largeCaves = false;
		
		private @Builder.Default final boolean stoneSpeleothems = false;
		private @Builder.Default final boolean sandstoneSpeleothems = false;

		private @Builder.Default final boolean sandReplacement = false;
		private @Builder.Default final boolean iceReplacement = false;

		private @Builder.Default final boolean icicles = false;

		private @Builder.Default final boolean skulls = false;
		private @Builder.Default final boolean cobwebs = false;
		private @Builder.Default final boolean spiderDens = false;

		private @Builder.Default final boolean vines = false;
		private @Builder.Default final boolean greenGlowshrooms = false;
		private @Builder.Default final boolean blueGlowshrooms = false;
		private @Builder.Default final boolean tallGreenGlowshrooms = false;
		
		private @Builder.Default final boolean leafPiles = false;

		private @Builder.Default final boolean wildWheat = false;
		private @Builder.Default final boolean wildPotatoes = false;
		private @Builder.Default final boolean wildCarrots = false;
		private @Builder.Default final boolean wildBeetroot = false;

		private @Builder.Default final boolean netherThorns = false;
		private @Builder.Default final boolean endThorns = false;
	}
	
	public static class BiomeGroupConfig {
		private @Getter String[] categories = null;
		private @Getter List<String> includedBiomes = new ArrayList<>();
		private @Getter List<String> excludedBiomes = new ArrayList<>();
		private @Getter BiomeConfig config;
		
		private BiomeGroupConfig(Category... categories) {
			this.categories = Arrays.stream(categories).map(category -> category.getName()).toArray(String[]::new);
		}
	}
}
