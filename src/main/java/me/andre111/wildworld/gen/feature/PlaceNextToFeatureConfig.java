/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import me.andre111.wildworld.Utils;
import net.minecraft.block.BlockState;
import net.minecraft.util.StringIdentifiable;
import net.minecraft.util.math.Direction;
import net.minecraft.world.gen.feature.FeatureConfig;

public class PlaceNextToFeatureConfig implements FeatureConfig {
	public static final Codec<Direction> directionCodec = StringIdentifiable.createCodec(Direction::values, Utils::directionByName);
	public static final Codec<PlaceNextToFeatureConfig> CODEC = RecordCodecBuilder.create((instance) -> {
		return instance.group(BlockState.CODEC.fieldOf("state").forGetter((placeNextToFeatureConfig) -> {
			return placeNextToFeatureConfig.state;
		}), BlockState.CODEC.fieldOf("attachState").forGetter((placeNextToFeatureConfig) -> {
			return placeNextToFeatureConfig.attachState;
		}), directionCodec.fieldOf("direction").forGetter((placeNextToFeatureConfig) -> {
			return placeNextToFeatureConfig.direction;
		}), directionCodec.fieldOf("searchDirection").forGetter((placeNextToFeatureConfig) -> {
			return placeNextToFeatureConfig.searchDirection;
		})).apply(instance, PlaceNextToFeatureConfig::new);
	});


	public final BlockState state;
	public final BlockState attachState;
	public final Direction direction;
	public final Direction searchDirection;

	public PlaceNextToFeatureConfig(BlockState state, BlockState attachState, Direction direction, Direction searchDirection) {
		this.state = state;
		this.attachState = attachState;
		this.direction = direction;
		this.searchDirection = searchDirection;
	}
}
