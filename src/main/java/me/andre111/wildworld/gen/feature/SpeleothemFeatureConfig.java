/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.world.gen.feature.FeatureConfig;

public class SpeleothemFeatureConfig implements FeatureConfig {
	public static final Codec<SpeleothemFeatureConfig> CODEC = RecordCodecBuilder.create((instance) -> {
		return instance.group(BlockState.CODEC.fieldOf("block").forGetter((speleothemFeatureConfig) -> {
			return speleothemFeatureConfig.block;
		}), BlockState.CODEC.fieldOf("attachBlock").forGetter((speleothemFeatureConfig) -> {
			return speleothemFeatureConfig.attachBlock;
		}), Codec.INT.fieldOf("maxLength").forGetter((speleothemFeatureConfig) -> {
			return speleothemFeatureConfig.maxLength;
		})).apply(instance, SpeleothemFeatureConfig::new);
	});
	
	public final BlockState block;
	public final BlockState attachBlock;
	public final int maxLength;

	public SpeleothemFeatureConfig(BlockState block, BlockState attachBlock, int maxLength) {
		this.block = block;
		this.attachBlock = attachBlock;
		this.maxLength = maxLength;
	}
}
