/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.carver;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.world.gen.ProbabilityConfig;
import net.minecraft.world.gen.carver.CaveCarver;

public class LargeCaveCarver extends CaveCarver {

	public LargeCaveCarver(Codec<ProbabilityConfig> codec, int i) {
		super(codec, i);
	}

	@Override
	protected float getTunnelSystemWidth(Random random) {
		return super.getTunnelSystemWidth(random) * 2.0f;
	}
	
	@Override
	protected double getTunnelSystemHeightWidthRatio() {
		return super.getTunnelSystemHeightWidthRatio() * 2.0;
	}
	
	@Override
	protected int getCaveY(Random random) {
      return random.nextInt(random.nextInt(80) + 8);
   }
}
