/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.block;

import me.andre111.wildworld.Config;
import me.andre111.wildworld.WildWorld;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.PlantBlock;
import net.minecraft.block.ShapeContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class ThornsBlock extends PlantBlock {
	// note: the code in onEntityCollision uses hardcoded (slightly different, smaller) box values for precise collision checking
	protected static final VoxelShape SHAPE = Block.createCuboidShape(2.0D, 0.0D, 2.0D, 14.0D, 14.0D, 14.0D);
	
	private final Block soilBlock;

	public ThornsBlock(Block soilBlock, Settings block$Settings_1) {
		super(block$Settings_1);
		
		this.soilBlock = soilBlock;
	}

	@Override
	protected boolean canPlantOnTop(BlockState blockState_1, BlockView blockView_1, BlockPos blockPos_1) {
		Block block_1 = blockState_1.getBlock();
		return block_1 == soilBlock;
	}

	@Override
	public VoxelShape getOutlineShape(BlockState blockState_1, BlockView blockView_1, BlockPos blockPos_1, ShapeContext context_1) {
		return SHAPE;
	}

	@Override
	public void onEntityCollision(BlockState blockState_1, World world_1, BlockPos blockPos_1, Entity entity_1) {
		if (entity_1 instanceof LivingEntity) {
			entity_1.slowMovement(blockState_1, new Vec3d(0.8, 0.75, 0.8));
			
			if (!world_1.isClient && (entity_1.lastRenderX != entity_1.getX() || entity_1.lastRenderZ != entity_1.getZ())) {
				// do precise collision check
				Box boxEntity = entity_1.getBoundingBox();
				Box boxBlock = new Box(blockPos_1.getX()+3/16.0, blockPos_1.getY(), blockPos_1.getZ()+3/16.0, blockPos_1.getX()+13/16.0, blockPos_1.getY()+14/16.0, blockPos_1.getZ()+13/16.0);
				if(boxEntity.intersects(boxBlock)) {
					// do movement check
					double double_1 = Math.abs(entity_1.getX() - entity_1.lastRenderX);
					double double_2 = Math.abs(entity_1.getZ() - entity_1.lastRenderZ);
					if (double_1 >= 0.001 || double_2 >= 0.001) {
						if(Config.getThornsDamage() > 0) {
							entity_1.damage(WildWorld.DAMAGESOURCE_THORNS, Config.getThornsDamage());
						}
					}
				}
			}
		}
	}
	
	@Override
	public void onBlockBreakStart(BlockState blockState_1, World world_1, BlockPos blockPos_1, PlayerEntity playerEntity_1) {
		if(!world_1.isClient) {
			float damage = 2.5f;
			if(playerEntity_1.getMainHandStack().getItem() == Items.SHEARS) {
				damage = 1f;
			}
			playerEntity_1.damage(WildWorld.DAMAGESOURCE_THORNS, damage);
		}
	}
}
