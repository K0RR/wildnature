# Wild World

## Project Page

The project page can be found on CurseForge: https://www.curseforge.com/minecraft/mc-mods/wild-world

## License

This mod is available under the Apache license Version 2.0.
